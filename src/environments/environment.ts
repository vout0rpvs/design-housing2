// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyA5Vb0s7p9GopZdtHeB9nCpHq27ZhshJ1w',
    authDomain: 'device-housing.firebaseapp.com',
    databaseURL: 'https://device-housing.firebaseio.com',
    projectId: 'device-housing',
    storageBucket: 'device-housing.appspot.com',
    messagingSenderId: '619824945063'
  }
};
