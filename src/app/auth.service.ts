import {
  Injectable
} from '@angular/core';
import {
  Router
} from '@angular/router';
import * as firebase from 'firebase/app';
import {
  AngularFireAuth
} from 'angularfire2/auth';
import {
  AngularFirestore,
} from 'angularfire2/firestore';

import {
  Observable
} from 'rxjs/Observable';
import {
  HttpClient,
  HttpHeaders
} from '@angular/common/http';
import {
  Subject
} from '../../node_modules/rxjs';
import {
  AngularFireDatabase
} from '../../node_modules/angularfire2/database';
import { EmailValidator } from '../../node_modules/@angular/forms';
import * as Rx from 'rxjs';
import { ToastrService } from 'ngx-toastr';

@Injectable()

export class AuthService {
  authState: any = null;

  themesRef = this.db.list('themes');
  // public user: Observable<firebase.User>;
  public userDetails: firebase.User = null;
  token: string = window.localStorage.getItem('user_token');
  user = new Subject < firebase.User > ();
  private root_url = 'https://device-housing.firebaseio.com';

  constructor(private fireAuth: AngularFireAuth,
    private toastr: ToastrService,
    private router: Router,
    private db: AngularFireDatabase) {
      this.fireAuth.authState.subscribe(auth => {
        this.authState = auth;
      });
    }
    // Returns true if user is logged in
  get authenticated(): boolean {
    return this.authState !== null;
  }

  // Returns current user data
  get currentUser(): any {
    return this.authenticated ? this.authState : null;
  }

  // Returns an observable of our user status
  get currentUserObservable(): any {
    return this.fireAuth.authState;
  }

  // Returns current user UID
  get currentUserId(): string {
    return this.authenticated ? this.authState.uid : '';
  }

  // Returns current user display name or Guest
  get currentUserDisplayName(): string {
    if (!this.authState) { return 'Guest'; } else { return this.authState['displayName'] || 'User without a Name'; }
  }

  credentialsLogin = value => {
        return this.fireAuth.auth.signInWithEmailAndPassword(value.email, value.password)
          .then(user => {
            this.authState = user;
            this.router.navigate(['admin']);
            this.toastr.success('You have successfully logged in');
        }).catch(error => {
         this.toastr.error('I am terribly sorry but ' + error.message + ' Please try again!');
        });
  }

  signOut(): void {
    this.fireAuth.auth.signOut();
    this.router.navigate(['login']);
    this.toastr.info('You logged out');
  }
  private updateUserData = () => {
    // Writes user name and email to realtime db
    // useful if your app displays information about users or for admin features

      const path = `users/${this.currentUserId}`; // Endpoint on firebase
      const data = {
                    email: this.authState.email,
                    name: this.authState.displayName
                  };

      this.db.object(path).update(data)
      .catch(error => console.log(error));
    }



  sendTheme = theme => {
    return new Promise((res, rej) => {
      res(this.themesRef.push(theme));
    });
  }
  getInfoModes = () => {
    const infoModesRef = this.db.list('/info_modes');
    return infoModesRef.snapshotChanges();
  }
  getAvailableThemes = () => {
    return this.themesRef.snapshotChanges();
  }
  getAvailableThemes2 = () => {
    return this.themesRef.valueChanges();
  }
  updateTheme = (key, theme) => {
    this.db.list(`/themes/`).snapshotChanges().subscribe(changes => {
      changes.map(c => {
        // tslint:disable-next-line:no-unused-expression
        if (c.payload.key === key) {
          c.payload.ref.update(theme);
          c.payload.ref.remove();
        } else {
          return;
        }
        this.toastr.success('Theme updated');
      });
    });
  }
  deleteTheme = (key: string) => {
    this.themesRef.remove(key);
  }
}
