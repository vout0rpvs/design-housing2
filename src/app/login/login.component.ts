import {
  Component,
  OnInit
} from '@angular/core';
import {
  ToastrService
} from 'ngx-toastr';
import {
  FormBuilder,
  Validators,
  FormGroup
} from '../../../node_modules/@angular/forms';
import {
  Router
} from '../../../node_modules/@angular/router';
import {
  AuthService
} from '../auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  errorMessage: String = '';
  constructor(private fb: FormBuilder,
    private toastr: ToastrService,
    private router: Router,
    private authService: AuthService) {
    this.createForm();
  }
  createForm = () => {
    this.loginForm = this.fb.group({
      email: ['', Validators.required],
      password: ['', Validators.required],
    });
  }

  private afterSignIn(): void {
    // Do after login stuff here, such router redirects, toast messages, etc.
    this.router.navigate(['/admin']);
  }
  credentialsLogin = value => {
    this.authService.credentialsLogin(value);
  }
  validation = (event , cred) => {
    if (event.target.value === '') {
      this.toastr.error('Please enter ' + cred );
    }
  }

  ngOnInit() {

  }

}
