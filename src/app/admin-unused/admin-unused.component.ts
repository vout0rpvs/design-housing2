import {
  Component,
  OnInit,
  Input,
  AfterViewChecked
} from '@angular/core';
import {
  AuthService
} from '../auth.service';
import {
  AngularFirestore
} from 'angularfire2/firestore';
import {
  FormGroup,
  FormBuilder,
  Validators,
} from '@angular/forms';
import {
  Router
} from '@angular/router';
import {
  AngularFireDatabase
} from 'angularfire2/database';
import * as firebase from 'firebase';
import {
  HttpClient
} from '../../../node_modules/@angular/common/http';
import * as Rx from 'rxjs';

@Component({
  selector: 'app-admin-unused',
  templateUrl: './admin-unused.component.html',
  styleUrls: ['./admin-unused.component.scss']
})
export class AdminUnusedComponent implements OnInit, AfterViewChecked {
  templates: any;
  // togglers
  bgch = false;
  inr = false;
  bgrAvailable = false;
  submitted = false;
  themeLoadTime = false;

  createThemeForm: FormGroup;
  templateForm: FormGroup;
  infocircle = document.querySelector('.infocircle');

  infoModes = {};
  availableThemes = [];
  availableThemesWithKeys: any;
  currentKey: any = null;

  customBgr: string;
  customInnerFace: string;


  background: any;
  casing: any;
  inner_circle: any;
  outer_circle: any;
  font: any;
  errorMessage = '';

  constructor(private fb: FormBuilder,
    public authService: AuthService,
    private af: AngularFireDatabase,
    private router: Router,
    private http: HttpClient) {
    localStorage.removeItem('firebase:previous_websocket_failure');
  }
  getInnerFace = item => {
    if (item) {
      this.bgrAvailable = true;
      this.customInnerFace = item;
      this.selectInnerFace(this.customInnerFace);
      const tCtrl = this.createThemeForm.controls;
      tCtrl.inner_image.setValue(this.customInnerFace);
      tCtrl.inner_circle.setValue(null);
    }
  }
  getBgr = item => {
    if (item) {
      this.bgrAvailable = true;
      this.customBgr = item;
      this.setBackgroundImage();
      const tCtrl = this.createThemeForm.controls;
      tCtrl.background_image.setValue(this.customBgr);
      tCtrl.background.setValue(null);
    }
  }
  saveColor = (name, value) => {
    this.createThemeForm.controls[name].setValue(value);
  }

  getThemes = () => {
    this.authService.getAvailableThemes().subscribe(changes => {
      this.availableThemesWithKeys = changes.map(c => ({
        key: c.payload.key,
        value: c.payload.val()
      }));
      this.availableThemes = Object.values(this.availableThemesWithKeys);
    });
  }
  selectInnerFace = url => {
    const inner = document.querySelector('#layer_3');
    inner['style'].background = 'url(' + url + '+)';
    inner['style'].backgroundSize = 'cover';
  }
  valueSwitch = (childFont, flexFlow, order, display, parentFontSize) => {
    const el = document.querySelector('.infocircle');
    const value = document.querySelector('.value-measured');
    value.childNodes[0]['style'].fontSize = childFont;
    el['style'].flexFlow = flexFlow;
    value['style'].order = order;
    value['style'].display = display;
    value['style'].fontSize = parentFontSize;

  }
  selectValuePositioning = (event, mode) => {
    if (event.target.checked || this.currentKey !== null) {
      switch (mode) {
        case 'default':
        this.valueSwitch('4rem', 'column', '3', 'initial', 'initial');
          break;
        case 'reverse':
        this.valueSwitch('4rem', 'column-reverse', 'initial', 'initial', 'initial');
          break;
        case 'value_first':
        this.valueSwitch('4rem', 'column', '1', 'initial', 'initial');
          break;
        case 'value_last_mini':
        this.valueSwitch('2rem', 'column', '6', 'initial', 'initial');
          break;
        case 'non_visible':
        this.valueSwitch('4rem', 'column', '6', 'none', 'initial');
          break;
      }
    }

  }
  private setBackgroundImage = () => {
    const tCtrl = this.createThemeForm.controls;
    tCtrl.background_image.setValue(this.customBgr);
    return null;
  }
  editTheme = index => {
    this.themeLoadTime = true;
    this.inr = this.bgch = false;
    // t for theme
    const tCtrl = this.createThemeForm.controls;
    const tCurrent = Object.values(this.availableThemesWithKeys)[index]['value'];
    this.customBgr = tCurrent.background_image;
    this.customInnerFace = tCurrent.inner_image;

    this.currentKey = this.availableThemesWithKeys[index]['key'];

    setTimeout(() => {
      this.themeLoadTime = false;
      this.createThemeForm.patchValue({
        theme_name: tCurrent.theme_name,
        background: tCurrent.background,
        background_image: (!tCurrent.background || (tCurrent.background && tCurrent.background_image))
        ? tCurrent.background_image : null,
        casing: tCurrent.casing,
        inner_circle: tCurrent.inner_circle ? tCurrent.inner_circle : null,
        inner_image: (!tCurrent.inner_circle || (tCurrent.inner_circle && tCurrent.inner_image))
        ? tCurrent.inner_image : null,
        outer_circle: tCurrent.outer_circle,
        font: tCurrent.font,
        mode: tCurrent.mode
      });
    }, 100);
    console.log(this.createThemeForm.value);

    setTimeout(() => {
      tCurrent.inner_image ? this.inr = true : this.inr = false;
      tCurrent.background_image ? this.bgch = true : this.bgch = false;
    }, 0);


    this.selectValuePositioning(event, tCurrent.mode);
  }
  updateTheme = () => {
    this.authService.updateTheme(this.currentKey, this.createThemeForm.value);
    setTimeout(() => {
      this.currentKey = null;
    }, 100);
  }
  deleteTheme = (event, index) => {
    event.stopPropagation();
    const key = this.availableThemesWithKeys[index].key;
    this.authService.deleteTheme(key);
    this.availableThemes.splice(index, 1);
    alert('Deleted successfully');
  }
  cancelUpdate = () => {
    this.createThemeForm.reset();
    this.currentKey = null;
    this.inr = this.bgch = false;
  }

  submitTheme = () => {
    this.submitted = true;
    if (this.createThemeForm.invalid) {
      console.log('this form is invalid');
    }
    console.log(this.createThemeForm.value);

    this.authService.sendTheme(this.createThemeForm.value).then(() => {
      this.createThemeForm.reset();
      this.getThemes();
    });
  }
  ngOnInit() {
    this.createThemeForm = this.fb.group({
      theme_name: [null, Validators.required],
      background: null,
      background_image: null,
      casing: [null, Validators.required],
      inner_circle: ' ',
      outer_circle: null,
      inner_image: null,
      font: [null, Validators.required],
      mode: [null, Validators.required]
    });
    this.getThemes();
    this.authService.getInfoModes().subscribe(modes => {
      this.infoModes = { ...modes
      };
    });
  }
  ngAfterViewChecked() {
    if (localStorage.getItem('currentTheme')) {
      const theme = JSON.parse(localStorage.getItem('currentTheme'));
      const body = document.querySelector('body');
      if (!theme.value.background || (theme.value.background && theme.value.background_image)) {
        body.style.background = 'url(' + theme.value.background_image + '+)';
        body.style.backgroundSize = 'cover';
      } else {
        body.style.background = theme.value.background;
      }
    }
  }

}
