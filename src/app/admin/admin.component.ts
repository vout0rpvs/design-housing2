import {
  Component,
  OnInit,
  Input,
  AfterViewChecked,
  NgZone,
  AfterViewInit,
} from '@angular/core';
import {
  AuthService
} from '../auth.service';
import {
  AngularFirestore
} from 'angularfire2/firestore';
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormArray,
  FormControl,
} from '@angular/forms';
import {
  AngularFireDatabase
} from 'angularfire2/database';
import {
  STAGES
} from './stages';
import {
  HttpClient
} from '../../../node_modules/@angular/common/http';
import * as Rx from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { last } from '../../../node_modules/rxjs/operators';
import { MqttService, IMqttMessage } from '../../../node_modules/ngx-mqtt';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {
  StageNames = ['Good', 'Moderate', 'UnhealthyFor', 'Unhealthy', 'Very Unhealthy', 'Hazardous'];
  defaultFace = '../../assets/images/DeviceHousing.png';

  templates: any;
  stages = STAGES;
  carouselStages: any = STAGES;
  // togglers
  toggler: boolean;
  bgch = false;
  submitted = false;

  createThemeForm: FormGroup;

  availableThemes = [];
  availableThemesWithKeys: any;
  currentKey: any = null;

  customBgr: string;
  customStateImage: string;

  background: any;
  font: any;
  top_position: any;
  left_position: any;
  errorMessage: any;

  defaultMqtt: any;
  private subscription: Rx.Subscription;
  public message: string;

  private _eNode: any;
  stateImage: string;

  constructor(private fb: FormBuilder,
    public authService: AuthService,
    private toastr: ToastrService,
    private _mqtt: MqttService) {
    localStorage.removeItem('firebase:previous_websocket_failure');
    this.font = 16;
    this.top_position = 75;
    this.left_position = 15;
    Rx.of(localStorage.getItem('currentTheme')).pipe(last()).subscribe(data => {
      const theme = JSON.parse(data);
      const body = document.querySelector('body');
      if (!theme.value.background || (theme.value.background && theme.value.background_image)) {
        body.style.background = 'url(' + theme.value.background_image + '+)';
        body.style.backgroundSize = 'cover';
      } else {
        body.style.background = theme.value.background;
      }
    });
    Rx.of(localStorage.getItem('defaultMqtt')).pipe(last()).subscribe(data => {
      this.defaultMqtt = JSON.parse(data);
      this._mqtt.connect({
        hostname: this.defaultMqtt.broker,
        port: parseInt(this.defaultMqtt.port, 10),
        protocol: 'ws'
      });
      this.subscribe(this.defaultMqtt.topic);
    });
  }
  subscribe(topic): void {
    this.subscription = this._mqtt.observe(topic).subscribe((message: IMqttMessage) => {
      this.message = message.payload.toString();
      const msg = parseInt(this.message, 10);

      if (msg <= 50) {
        this.stateImage = this.carouselStages[0].url;
      } else if (msg <= 100) {
        this.stateImage = this.carouselStages[1].url;
      } else if (msg <= 150) {
        this.stateImage = this.carouselStages[2].url;
      } else if (msg <= 200) {
        this.stateImage = this.carouselStages[3].url;
      } else if (msg <= 300) {
        this.stateImage = this.carouselStages[4].url;
      } else if (msg <= 500) {
        this.stateImage = this.carouselStages[5].url;
      } else {
        this.stateImage = this.carouselStages[0].url;
      }
    });
  }
  getBgr = item => {
    this.customBgr = item;
    this.setBackgroundImage();
    const tCtrl = this.createThemeForm.controls;
    tCtrl.background_image.setValue(this.customBgr);
    tCtrl.background.setValue(null);
  }
  getThemes = () => {
    this.authService.getAvailableThemes().subscribe(changes => {
      this.availableThemesWithKeys = changes.map(c => ({
        key: c.payload.key,
        value: c.payload.val()
      }));
      this.availableThemes = Object.values(this.availableThemesWithKeys);
    });
  }
  getStateImage = (image, stage) => {
    this._eNode = event.target['parentNode'];
    const childNodes = event.target['parentNode']['parentNode']['childNodes'];

    this.customStateImage = image;
    if (this._eNode.classList.contains('template-list')) {
      return;
    }
    const tCtrl = this.createThemeForm.controls;
    this.stages.forEach((curStage, index) => {
      if (curStage.name === stage) {
        if (childNodes.length >= 3) {
          curStage.url = this.customStateImage;
          this.carouselStages[index].url = this.customStateImage;
          Array.from(childNodes).forEach(node => {
            if (node['classList'] && node['classList'].contains('chosen-' + stage)) {
              node['classList'].remove('chosen-' + stage);
            }
          });
          this._eNode.classList.add('chosen-' + stage);
          tCtrl.stages.value[index] = curStage;
        } else {
          if (this._eNode.classList.contains('chosen-' + stage)) {
            curStage.url = this.defaultFace;
            this.carouselStages[index].url = this.defaultFace;

            this._eNode.classList.remove('chosen-' + stage);
          } else {
            curStage.url = this.customStateImage;
            this.carouselStages[index].url = this.customStateImage;
            this._eNode.classList.add('chosen-' + stage);
          }
          tCtrl.stages.value[index] = curStage;
        }
      }
    });

  }
  saveColor = (name, value) => {
    this.createThemeForm.controls[name].setValue(value);
  }
  private setBackgroundImage = () => {
    const tCtrl = this.createThemeForm.controls;
    tCtrl.background_image.setValue(this.customBgr);
    return null;
  }
  editTheme = index => {
    Array.from(document.querySelectorAll('.theme')).forEach(item => {
      item['classList'].remove('selected');
    });
    event.target['classList'].add('selected');
    const editObservable = Rx.from(document.querySelectorAll('.template-list-item'));
    // t for theme
    const tCtrl = this.createThemeForm.controls;
    const tCurrent = Object.values(this.availableThemesWithKeys)[index]['value'];
    this.carouselStages = tCurrent.stages;
    this.customBgr = tCurrent.background_image;
    this.currentKey = this.availableThemesWithKeys[index]['key'];

    setTimeout(() => {
      this.createThemeForm.patchValue({
        theme_name: tCurrent.theme_name,
        background: tCurrent.background ? tCurrent.background : null,
        background_image: (!tCurrent.background || (tCurrent.background && tCurrent.background_image)) ?
          tCurrent.background_image : null,
        stages: tCurrent.stages,
        font: tCurrent.font,
        font_color: tCurrent.font_color,
        top_position: tCurrent.top_position,
        left_position: tCurrent.left_position,
      });
    }, 100);


    editObservable.pipe().subscribe(htmlEl => {
      tCurrent.stages.forEach(stage => {
        if (stage.url === htmlEl['childNodes'][0]['src']) {
          htmlEl.classList.add('chosen-' + stage.name);
        } else {
          htmlEl.classList.remove('chosen-' + stage.name);
        }
      });

    });
    tCurrent.background ? this.bgch = false : this.bgch = true;
  }
  updateTheme = () => {
    this.authService.updateTheme(this.currentKey, this.createThemeForm.value);
    setTimeout(() => {
      this.currentKey = null;
    }, 100);
  }
  deleteTheme = (event, index) => {
    event.stopPropagation();
    const key = this.availableThemesWithKeys[index].key;
    this.authService.deleteTheme(key);
    this.availableThemes.splice(index, 1);
    this.toastr.success('Deleted successfully');
    this.cancelUpdate();
  }
  cancelUpdate = () => {
    Array.from(document.querySelectorAll('.template-list-item')).forEach(item => {
      item.classList.remove('chosen', 'chosen-g', 'chosen-m', 'chosen-ufg', 'chosen-u', 'chosen-vu', 'chosen-h');
    });
    const vl = Array.from(document.querySelectorAll('.value-measured'));
    vl.forEach(v => {
      v['style'].top = '75%';
      v['style'].left = '15%';
      v['style'].fontSize = 'initial';
    });

    this.stages.forEach((stage, index) => {
      stage.url = this.defaultFace;

    });
    console.log(this.carouselStages);
    this.createThemeForm.reset();
    this.currentKey = null;
    this.bgch = false;
  }

  submitTheme = () => {

    this.submitted = true;
    if (this.createThemeForm.invalid) {
      alert('This form is invalid, please fill in the required fields');
      return;
    }
    this.authService.sendTheme(this.createThemeForm.value).then(() => {
      this.toastr.info('Theme saved');
      this.createThemeForm.reset();
      this.getThemes();
      this.stages.map(stage => {
        stage.name = stage.name;
        stage.url = this.defaultFace;
      });
    });
    this.cancelUpdate();
  }

  onThemeChange = event => {
    if (event.target.value === '') {
      this.toastr.error('Please Enter theme name');
    }
  }
  onLeftPosChange = event => {
    if (event.target.value < -25) {
      event.target.value =  -25;
      this.toastr.warning('Minimum value reached');
    } else if (event.target.value > 55) {
      event.target.value =  55;
      this.toastr.warning('Maximum value reached');

    }
  }
  onTopPosChange = event => {
    if (event.target.value < 10) {
      event.target.value = 10;
      this.toastr.warning('Minimum value reached');
    } else if (event.target.value > 80) {
      event.target.value =  80;
      this.toastr.warning('Maximum value reached');
    }
  }
  onValueSizeChange = event => {
    if (event.target.value < 12) {
      event.target.value =  12;
      this.toastr.warning('Minimum value reached');
    } else if (event.target.value > 72) {
      event.target.value =  72;
      this.toastr.warning('Maximum value reached');
    }
  }

  ngOnInit() {
    this.createThemeForm = this.fb.group({
      theme_name: [null, Validators.required],
      background: null,
      background_image: null,
      font: null,
      font_color: null,
      top_position: null,
      left_position: null,
      stages: new FormArray([
        new FormControl({
          name: 'g',
          url: this.defaultFace
        }),
        new FormControl({
          name: 'm',
          url: this.defaultFace
        }),
        new FormControl({
          name: 'ufg',
          url: this.defaultFace
        }),
        new FormControl({
          name: 'u',
          url: this.defaultFace
        }),
        new FormControl({
          name: 'vu',
          url: this.defaultFace
        }),
        new FormControl({
          name: 'h',
          url: this.defaultFace
        }),
      ])
    });
    this.getThemes();
  }

}
