import {
  Component,
  OnInit,
  OnDestroy,
  AfterViewChecked
} from '@angular/core';
import {
  FormBuilder,
  FormGroup
} from '../../../../node_modules/@angular/forms';
import {
  UploadService
} from '../../uploads/shared/upload.service';
import {
  MqttService,
  IMqttMessage
} from '../../../../node_modules/ngx-mqtt';
import {
  Subscription
} from '../../../../node_modules/rxjs';
import { ToastrService } from 'ngx-toastr';
import { last, first } from 'rxjs/operators';



@Component({
  selector: 'app-mqtt-test',
  templateUrl: './mqtt-test.component.html',
  styleUrls: ['./mqtt-test.component.scss']
})
export class MqttTestComponent implements OnDestroy, OnInit {
  topic: any;
  publish: any;
  isDefault = false;
  connectionState: any;

  subscription: Subscription;

  connected = false;
  subscribed = false;

  default: false;
  defaultConn = {};
  focused = false;
  subscribedTopic: string;

  mqttForm: FormGroup;
  messages = [];
  mqtts = [];
  advanced: boolean;
  status = [];
  conStatus: boolean;
  protocol = ['wss', 'ws', 'mqtt', 'mqtts', 'tcp', 'ssl', 'wx', 'wxs'];
  constructor(private fb: FormBuilder,
    private uploadService: UploadService,
    private _mqtt: MqttService,
    private toastr: ToastrService) {
    this.mqttForm = this.fb.group({
      name: '',
      broker: '',
      protocol: 'ws',
      port: '',
      user: null,
      pass: null,
      topic: '',
      publish: ''
    });
  }
  connect = c => {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
    this._mqtt.connect({
      hostname: c.broker.value ? c.broker.value : ' ',
      port: +c.port.value ? c.port.value : ' ',
      protocol: 'ws',
      username: c.user.value,
      password: c.pass.value
    });

    this._mqtt.state.subscribe(state => {
      this.connectionState = state;
      if (state === 2) {
        this.conStatus = true;
        this.connected = true;
      } else {
        this.conStatus = false;
        this.connected = false;
      }
    });
  }
  onKeyDown = event => {
    return (event.ctrlKey || event.altKey ||
      (47 < event.keyCode && event.keyCode < 58 && event.shiftKey === false) ||
      (95 < event.keyCode && event.keyCode < 106) ||
      (event.keyCode === 8) || (event.keyCode === 9) ||
      (event.keyCode > 34 && event.keyCode < 40) ||
      (event.keyCode === 46));
  }
  subscribe = topic => {
    this.messages = [];
    this.publish = '';
    this.subscribed = true;
    this.toastr.info('Subscribed to ' + topic);
    this.subscription = this._mqtt.observe(topic).subscribe((message: IMqttMessage) => {

      this.messages.push(message.payload.toString());
    });
  }


  sendMsg = (topic, value) => {
    this._mqtt.unsafePublish(topic, value, {
      qos: 0,
      retain: true
    });
  }

  unsubscribe = topic => {
    if (this.subscription) {
      this.subscription.unsubscribe();
      this.toastr.info('Unsubscribed from ' + topic);
    } else {
      return;
    }
  }
  reload = url => {
    window.location.href = url;
  }
  saveConnection = (data) => {
    data = {
      ...data,
      topic: this.topic
    };
    this.uploadService.sendConnection(data);
  }
  selectAsDefault = data => {
    this.isDefault = true;
    data = {
      ...data,
      topic: this.topic,
      default: true
    };
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
    localStorage.setItem('defaultMqtt', JSON.stringify(data));
    this.mqtts.forEach((mqt, index) => {
      if (mqt.payload.val().name === data.name) {
        const conn = < HTMLElement > document.querySelectorAll('.connection')[index];
        conn.classList.add('active');
        this.editConnection(mqt.payload.val(), index);
      }
    });
  }
  deleteConnection = key => {
    this.uploadService.removeConnection(key);
  }
  editConnection = (conn, index) => {

    this.mqttForm.patchValue({
      name: conn.name,
      broker: conn.broker,
      protocol: 'ws',
      port: conn.port,
      user: conn.user ? conn.user : null,
      pass: conn.pass ? conn.pass : null,
      topic: conn.topic,
      publish: ''
    });
    this.topic = conn.topic ? conn.topic : null;
    this.connected = false;
    this.subscribed = false;
    this.focused = true;
    for (let i = 0; i < document.querySelectorAll('.connection').length; i++) {
      document.querySelectorAll('.connection')[i].classList.remove('active');
    }
    const el = < HTMLElement > document.querySelectorAll('.connection')[index];
    el.classList.add('active');
  }

  ngOnInit(): void {
    this.uploadService.getConnection().subscribe(mqtt => {
      this.mqtts = mqtt;

      this.defaultConn = JSON.parse(localStorage.getItem('defaultMqtt'));
      this.mqtts.forEach((mqt, index) => {
        if (mqt.payload.val().name === this.defaultConn['name']) {
          setTimeout(() => {
            const conn = < HTMLElement > document.querySelectorAll('.connection')[index];
            conn.classList.add('active');
            this.focused = true;
            this.isDefault = true;
            this.editConnection(mqt.payload.val(), index);
          }, 100);

        }
      });
    });
    if (localStorage.getItem('currentTheme')) {
      const theme = JSON.parse(localStorage.getItem('currentTheme'));
      const body = document.querySelector('body');
      if (theme.value.background === null || theme.value.background === '' || !theme.value.background) {
        body.style.background = 'url(' + theme.value.background_image + '+)';
        body.style.backgroundRepeat = 'no-repeat';
      } else {
        body.style.background = theme.value.background;
      }
    }
  }
  ngOnDestroy(): void {
    if (this.connectionState === 0) {
      this._mqtt.disconnect();
    }
  }
}
