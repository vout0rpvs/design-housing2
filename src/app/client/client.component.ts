import {
  Component,
  OnInit,
  Inject,
  Input,
  ViewChild,
  ElementRef,
  Renderer,
  NgZone,
  OnDestroy
} from '@angular/core';
import {
  AuthService
} from '../auth.service';

import {
  UploadService
} from '../uploads/shared/upload.service';
import {
  MqttService,
  IMqttMessage
} from '../../../node_modules/ngx-mqtt';
import {
  Subscription
} from '../../../node_modules/rxjs';
import * as Rx from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { filter, first } from '../../../node_modules/rxjs/operators';

export interface Foo {
  bar: string;
}
@Component({
  selector: 'app-client',
  templateUrl: './client.component.html',
  styleUrls: ['./client.component.scss']
})
export class ClientComponent implements OnInit, OnDestroy {
  @Input() somedata: any;
  @ViewChild('circle') circle: ElementRef;
  private subscription: Subscription;
  public message: string;

  public defaultMqtt: any;

  public showThemes = false;

  public themable: any;
  public outer: any;
  public inner: any;
  public stateImage: any;

  public background: any;
  public title: any;
  public descriptor: any;
  public recommendation: any;
  public stages = [];
  public currentTheme: any;
  public currentFace = {
    title: 'AQI',
    value: Math.floor(Math.random() * 50) + 1,
    descriptor: 'Swiss Alpine Air',
    recommendation: 'WHO10',
    color1: '#00E400',
    color2: 'green'
  };
  private availableThemes = [];

  constructor(private authService: AuthService,
    private _mqtt: MqttService,
    private toastr: ToastrService) {}
  subscribe(topic): void {
    this.subscription = this._mqtt.observe(topic)
    .subscribe((message: IMqttMessage) => {
      this.message = message.payload.toString();
      const msg = parseInt(this.message, 10);
      if (msg <= 50) {
        this.stateImage = this.stages[0].url;
      } else if (msg <= 100) {
        this.stateImage = this.stages[1].url;
      } else if (msg <= 150) {
        this.stateImage = this.stages[2].url;
      } else if (msg <= 200) {
        this.stateImage = this.stages[3].url;
      } else if (msg <= 300) {
        this.stateImage = this.stages[4].url;
      } else if (msg <= 500) {
        this.stateImage = this.stages[5].url;
      } else {
        this.stateImage = this.stages[0].url;
      }
    });
  }
  getThemes() {
    this.authService.getAvailableThemes().pipe(first()).subscribe(changes => {
      console.log(changes);
      this.availableThemes = changes.map(c => ({
        key: c.payload.key,
        value: c.payload.val()
      }));
      this.availableThemes = Object.values(this.availableThemes);
    });
  }
  transferCurrentTheme = theme => {
    this.currentTheme = theme;
    Array.from(document.querySelectorAll('.span-theme')).forEach(item => {
      item['classList'].remove('selected');
    });
    event.target['classList'].add('selected');
  }
  applyTheme = (theme) => {
    const value = document.querySelector('.value-measured');
    this.stages = [];
    const bgr = document.querySelector('.main-container');

    if (!theme.value.background ||
       (theme.value.background && theme.value.background_image)) {
      bgr['style'].background = 'url(' + theme.value.background_image + '+)';
      bgr['style'].backgroundSize = 'cover';
    } else {
      bgr['style'].background = theme.value.background;
    }
    this.stages = theme.value.stages;
    this.preload(this.stages);
    value['style'].fontSize = theme.value.font ? theme.value.font + 'px' : 'initial';
    value['style'].color = theme.value.font_color ? theme.value.font_color : 'initial';
    value['style'].top = theme.value.top_position || theme.value.top_position === 0 ? theme.value.top_position + '%' : '75%';
    value['style'].left = theme.value.top_position || theme.value.left_position === 0 ? theme.value.left_position + '%' : '15%';
    localStorage.setItem('currentTheme', JSON.stringify(theme));
    this.toastr.success(theme.value.theme_name + ' is applied');

  }
  applyCurrent = () => {
    if (localStorage.getItem('currentTheme')) {
      const theme = JSON.parse(localStorage.getItem('currentTheme'));
      const sub = Rx.from(document.querySelectorAll('.span-theme'));
      sub.pipe(filter(res => res.textContent === theme.value.theme_name))
      .subscribe(res => {
          res.classList.add('selected');
      });
    }
  }
  preload = argss => {
    const images = new Array();
    for (let i = 0; i < argss.length; i++) {
      images[i] = new Image();
      images[i].src = argss[i].url;
    }
  }
  ngOnInit() {

    this.getThemes();
    if (localStorage.getItem('defaultMqtt')) {
      this.defaultMqtt = JSON.parse(localStorage.getItem('defaultMqtt'));
      this._mqtt.connect({
        hostname: this.defaultMqtt.broker,
        port: parseInt(this.defaultMqtt.port, 10),
        protocol: 'ws'
      });
      this.subscribe(this.defaultMqtt.topic);
    }
      if (localStorage.getItem('currentTheme')) {
        const theme = JSON.parse(localStorage.getItem('currentTheme'));
        this.applyTheme(theme);

      }
  }
  ngOnDestroy(): void {
    this._mqtt.disconnect();
  }

}
