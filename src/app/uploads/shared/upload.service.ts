import {
  Injectable
} from '@angular/core';
import {
  Upload
} from './upload';
import * as firebase from 'firebase/app';

import {
  AngularFireDatabase
} from '../../../../node_modules/angularfire2/database';
import {
  AngularFirestore
} from '../../../../node_modules/angularfire2/firestore';


@Injectable()
export class UploadService {

  constructor(private af: AngularFirestore, private db: AngularFireDatabase) {}
  private basePath = '/uploads';
  private uploadTask: firebase.storage.UploadTask;

  pushBackground = (upload: Upload) => {
    const storageRef = firebase.storage().ref();
    this.uploadTask = storageRef.child(`${this.basePath}/backgrounds/${upload.file.name}`).put(upload.file);
    this.uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED,
      () => {
        upload.progress = (this.uploadTask.snapshot.bytesTransferred / this.uploadTask.snapshot.totalBytes) * 100;
      }, err => {
        console.log(err);
      },
      () => {
        upload.url = this.uploadTask.snapshot.metadata.fullPath;
        upload.name = upload.file.name;
        this.saveBackground(upload);
      });
  }
  pushAqiStateImage = (upload: Upload, stage) => {
    const storageRef = firebase.storage().ref();
    this.uploadTask = storageRef.child(`${this.basePath}/state-images/${stage}/${upload.file.name}`).put(upload.file);
    this.uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED,
      () => {
        // upload in progress
        upload.progress = (this.uploadTask.snapshot.bytesTransferred / this.uploadTask.snapshot.totalBytes) * 100;
      }, err => {
        console.log(err);
      },
      () => {
        upload.url = this.uploadTask.snapshot.metadata.fullPath;
        upload.name = upload.file.name;
        this.saveStateImage(upload, stage);
      });
  }
  private saveBackground(upload: Upload) {
    this.db.list(`${this.basePath}/backgrounds`).push(upload);
  }
  private saveStateImage(upload: Upload, stage) {
    this.db.list(`${this.basePath}/state-images/${stage}`).push(upload);
  }
  sendConnection(data) {
    this.db.list(`mqtt/`).push(data);
  }
  saveSelectedAsADefaultConnection(data) {
    this.db.list('default/').remove();
    this.db.list('default/').push(data);
  }
  getDefault = () => {
    return this.db.object('default').valueChanges();
  }
  updateTheme = (key, theme) => {
    this.db.list(`/themes/`).snapshotChanges().subscribe(changes => {
      changes.map(c => {
        // tslint:disable-next-line:no-unused-expression
        if (c.payload.ref.key === key) {
          c.payload.ref.update(theme);
          c.payload.ref.remove();
        } else {
          return;
        }
      });
    });
  }
  getConnection() {
    return this.db.list('mqtt/').snapshotChanges();
  }
  removeConnection(key: string) {
    return this.db.list('mqtt/').remove(key);
  }
  deleteUpload(upload: Upload) {
    this.deleteFileData(upload.$key)
      .then(() => {
        this.deleteFileStorage(upload.name);
      }).catch(err => {
        console.log(err);
      });
  }
  deleteFileData(key: string) {
    return this.db.list(`${this.basePath}/`).remove(key);
  }
  private deleteFileStorage(name: string) {
    const storageRef = firebase.storage().ref();
    storageRef.child(`${this.basePath}/${name}`).delete();

  }

}
