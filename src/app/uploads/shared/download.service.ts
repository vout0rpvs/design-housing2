import {
  Injectable
} from '@angular/core';
import {
  Upload
} from './upload';
import * as firebase from 'firebase/app';
import {
  AngularFireDatabase
} from '../../../../node_modules/angularfire2/database';
import {
  AngularFirestore
} from '../../../../node_modules/angularfire2/firestore';
import {
  Observable
} from '../../../../node_modules/rxjs';
import {
  AngularFireStorage
} from '../../../../node_modules/angularfire2/storage';
import {
  HttpClient
} from '../../../../node_modules/@angular/common/http';
import { AngularFireAuth } from '../../../../node_modules/angularfire2/auth';

@Injectable()
export class DownloadService {
  private basePath = '/uploads';
  private uploadTask: firebase.storage.UploadTask;

  profileUrl: Observable < string | null > ;
  constructor(private af: AngularFirestore,
    private storage: AngularFireStorage,
    private db: AngularFireDatabase,
    private http: HttpClient,
    private fireAuth: AngularFireAuth) {}

  getBackgroundList() {
    const urls = [];
    const items = [];
    const itemsRef = this.db.list('/uploads/backgrounds');
      const storageRef = firebase.storage().ref();
      itemsRef.valueChanges().subscribe(changes => {
        changes.map(c => {
          items.push(c);
        }, err => console.log('Something went wrong', err.message));
        items.map(item => {
          storageRef.child(item.url).getDownloadURL().then(res => {
            urls.push(res);
          }, err => console.log('Something terrible happened', err.message));
        });
      });
    return new Promise((res, rej) => {
        res(urls);
        rej(new Error('Something awful happenned'));
    });
  }
  getStateImagesList(state) {
    const urls = [];
    const items = [];
    const itemsRef = this.db.list(`/uploads/state-images/${state}`);
      const storageRef = firebase.storage().ref();
      itemsRef.valueChanges().subscribe(changes => {
        changes.map(c => {
          storageRef.child(c['url']).getDownloadURL().then(res => {
            urls.push(res);
          }, err => console.log('Something terrible happened', err.message));
        }, err => console.log('Something went wrong', err.message));
      });
    return new Promise((res, rej) => {
        res(urls);
        rej(new Error('Something awful happenned'));
    });
  }
  deleteBackground(key) {
   this.db.list(`/uploads/backgrounds/${key}`).remove().then(res => {
     console.log(res);
   });
    const storageRef = firebase.storage().ref();

  }




}
