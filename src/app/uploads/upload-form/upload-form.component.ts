import { Component } from '@angular/core';
import {UploadService} from '../shared/upload.service';
import { Upload } from '../shared/upload';
import * as _ from 'lodash';

@Component({
  selector: 'app-upload-form',
  templateUrl: './upload-form.component.html',
  styleUrls: ['./upload-form.component.css']
})
export class UploadFormComponent {
  selectedFiles: FileList;
  selectedStage: any;
  currentUpload: Upload;
  constructor(private upService: UploadService) { }

  detectFiles = event => {
    this.selectedFiles = event.target.files;
  }
  uploadBackground = () => {
    const file = this.selectedFiles.item(0);
    this.currentUpload = new Upload(file);
    this.upService.pushBackground(this.currentUpload);
  }
  uploadStateImage = (selectedStage) => {
    const file = this.selectedFiles.item(0);
    this.currentUpload = new Upload(file);
    if (selectedStage) {
      this.upService.pushAqiStateImage(this.currentUpload, selectedStage);
    } else {
      alert('Please select stage to upload image to');
    }

  }

}
