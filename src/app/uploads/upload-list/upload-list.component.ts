import {
  Component,
  EventEmitter,
  Output,
  Input,
  OnInit,
  AfterViewInit,
  ViewChildren,
  QueryList
} from '@angular/core';
import {
  DownloadService
} from '../shared/download.service';
import * as Rx from 'rxjs';
import {
  map,
  filter
} from '../../../../node_modules/rxjs/operators';

@Component({
  selector: 'app-upload-list',
  templateUrl: './upload-list.component.html',
  styleUrls: ['./upload-list.component.scss']
})
export class UploadListComponent implements AfterViewInit {
  public backgrounds: any = [];
  public stateImages: any = [];
  public elRefs: any = [];
  public stages: any = [];
  @Output() customBackgroundUrl = new EventEmitter < string > ();
  @Output() clearForm = new EventEmitter < any > ();

  @Input() chosen: any;
  @Input() openState: any;
  @Input() whatToLoad: string;

  @ViewChildren('list') bgrNodes: QueryList < any > ;
  @ViewChildren('listState') stateNodes: QueryList < any > ;

  constructor(private dService: DownloadService) {
    this.backgrounds = [];
    this.stateImages = [];
    this.dService.getBackgroundList().then(res => {
      this.backgrounds = res;
    });
  }
  emitData = (url, event) => {
    event.stopPropagation();
    this.customBackgroundUrl.emit(url);
    if (this.whatToLoad === 'backgrounds') {
      Array.from(document.querySelectorAll('.chosen')).forEach(item => {
        item.classList.remove('chosen');
      });
      event.target.parentNode.classList.add('chosen');
    }
  }
  ngAfterViewInit(): void {

    this.bgrNodes.changes
      .pipe(map(refs => refs.map(ref => ref.nativeElement)),
        filter(nativeEl => nativeEl.parentNode && nativeEl.src === this.chosen))
      .subscribe(nativeEl => {
        nativeEl.parentNode.classList.add('chosen');
      });
    this.dService.getStateImagesList(this.openState).then(res => {
      this.stateImages = res;
    });
  }
}
