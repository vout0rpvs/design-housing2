import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule, Injectable } from '@angular/core';
import { ColorPickerModule } from 'ngx-color-picker';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { ToastrModule } from 'ngx-toastr';

import { AppComponent } from './app.component';
import { RouterModule, Routes } from '@angular/router';
import { Ng2CarouselamosModule } from 'ng2-carouselamos';

import {AngularFireModule} from 'angularfire2';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFireDatabaseModule, AngularFireDatabase } from 'angularfire2/database';
import { AngularFireStorageModule } from 'angularfire2/storage';
import { environment } from '../environments/environment';

import { LoginComponent } from './login/login.component';

import { IMqttMessage, MqttModule, IMqttServiceOptions } from 'ngx-mqtt';

import { DownloadService } from './uploads/shared/download.service';
import { AuthGuardService } from './auth-guard.service';
import { UploadService } from './uploads/shared/upload.service';
import { AuthService } from './auth.service';
import { HttpClientModule } from '@angular/common/http';

import { AdminComponent } from './admin/admin.component';
import { MainComponent } from './main/main.component';
import { UploadListComponent } from './uploads/upload-list/upload-list.component';
import { UploadFormComponent } from './uploads/upload-form/upload-form.component';
import { MqttTestComponent } from './mqtt/mqtt-test/mqtt-test.component';
import { AdminUnusedComponent } from './admin-unused/admin-unused.component';
import { ClientComponent } from './client/client.component';

const Routes: Routes = [
  {path: 'admin', component: AdminComponent, canActivate: [AuthGuardService]},
  {path: 'admin-unused', component: AdminUnusedComponent, canActivate: [AuthGuardService]},
  {path: 'login', component: LoginComponent },
  {path: 'mqtt', component: MqttTestComponent },
  {path: 'main-admin', component: MainComponent },
  {path: '', component: ClientComponent },
];


@NgModule({
  declarations: [
    AppComponent,
    AdminComponent,
    MainComponent,
    LoginComponent,
    UploadListComponent,
    UploadFormComponent,
    MqttTestComponent,
    AdminUnusedComponent,
    ClientComponent
  ],
  imports: [
    MqttModule.forRoot({
      connectOnCreate: false
    }),
    Ng2CarouselamosModule,
    BrowserModule,
    BrowserAnimationsModule, // required animations module
    ToastrModule.forRoot(),
    ColorPickerModule,
    ReactiveFormsModule,
    RouterModule.forRoot(Routes),
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule, // imports firebase/firestore, only needed for database features
    AngularFireAuthModule, // imports firebase/auth, only needed for auth features,
    AngularFireDatabaseModule,
    AngularFireStorageModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [
    AuthService,
    AngularFireDatabase,
    AuthGuardService,
    UploadService,
    DownloadService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
