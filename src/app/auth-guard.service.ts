import { Injectable } from '@angular/core';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { AuthService } from './auth.service';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/take';

@Injectable()
export class AuthGuardService implements CanActivate {
    constructor(private router: Router, private authService: AuthService) { }
    canActivate(
        next: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Observable<any> | boolean {
          if (this.authService.authenticated) { return true; }

          return this.authService.currentUserObservable
             .take(1)
             .map(user => !!user)
             .do(loggedIn => {
               if (!loggedIn) {
                 console.log('Access Denied, please log in');
                 this.router.navigate(['/login']);
               }
             });
}
}
